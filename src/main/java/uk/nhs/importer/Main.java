/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.nhs.importer;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 *
 * @author Tim Coates
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main instance = new Main();
        instance.run();
    }

    /**
     * Non static start point.
     * 
     */
    private void run() {
        File folder = new File("/home/tico3/importer/src/main/resources");
        //File folder = new File(".");
        File[] listOfFiles = folder.listFiles();
        
        System.out.println("Found: " + listOfFiles.length + " files\n");
        
        // Here we get the mongo collection
        Mongo mongoClient = new Mongo("localhost", 28015);
        DB db = mongoClient.getDB("mydb");
        DBCollection collection = db.getCollection("profiles");
        
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                String json = readFile(listOfFiles[i]);
                if (json != null) {
                    if (insert(json, collection)) {
                        if (listOfFiles[i].delete()) {
                            System.out.println("File: " + listOfFiles[i] + " imported and deleted.");
                        } else {
                            System.out.println("ERROR: File delete failed.");
                        }
                    } else {
                        System.out.println("ERROR: File import failed for: " + listOfFiles[i].getName());
                    }
                } else {
                    System.out.println("ERROR: File read failed for: " + listOfFiles[i].getName());
                }
            }
        }
    }

    /**
     * Method to read the entire contents of a given file into a String.
     * 
     * @param thisFile
     * @return 
     */
    private String readFile(File thisFile) {
        String contents = "";
        try (FileInputStream fis = new FileInputStream(thisFile)) {

            //System.out.println("Total file size to read (in bytes) : " + fis.available());

            int content;
            while ((content = fis.read()) != -1) {
                contents = contents + (char) content;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contents;
    }

    /**
     * Method to put the object in the json string into the specified collection.
     * 
     * @param json
     * @param collection
     * @return 
     */
    private boolean insert(String json, DBCollection collection) {
        DBObject dbObject = (DBObject)JSON.parse(json);
        if(dbObject.get("id") == null) {
            dbObject.put("id", UUID.randomUUID().toString());
        } else {
            if(dbObject.get("id").equals("")) {
                dbObject.put("id", UUID.randomUUID().toString());
            }
        }
        collection.insert(dbObject);
        return true;
    }

}
